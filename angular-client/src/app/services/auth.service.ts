import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthService {
    authToken: any;
    user: any;

    constructor(
        private http: HttpClient,
        public jwtHelper: JwtHelperService
    ) {
    }

    getHttpHeaders(authToken = null) {
        let headers = {
            'Content-Type':  'application/json'
        };
        if (authToken) {
            headers['Authorization'] = authToken;
        }
        return {
            headers: new HttpHeaders(headers),
            port: 3000
        }
    }

    registerUser(user) {
        let option = this.getHttpHeaders();
        return this.http.post('http://localhost:3000/users/register', user, option);
    }

    authenticateUser(user) {
        let option = this.getHttpHeaders();
        return this.http.post('http://localhost:3000/users/login', user, option);
    }

    getProfile() {
        this.loadToken();
        let option = this.getHttpHeaders(this.authToken);
        return this.http.get('http://localhost:3000/users/profile', option);
    }

    storeUserData(token, user) {
        localStorage.setItem('id_token', token);
        localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
    }

    loadToken() {
        this.authToken = localStorage.getItem('id_token');
    }

    loggedIn() {
        this.loadToken();
        return this.authToken ? !this.jwtHelper.isTokenExpired() : false;

        return this.jwtHelper.isTokenExpired();
    }

    logout() {
        this.authToken = null;
        this.user = null;
        localStorage.clear();
    }
}
