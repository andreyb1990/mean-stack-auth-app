import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from "@angular/router";
import {NgFlashMessageService} from 'ng-flash-messages';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    constructor(
        private authService: AuthService,
        private router: Router,
        private flashMessage: NgFlashMessageService,
    ) {

    }

    ngOnInit() {
    }

    onLogoutClick() {
        this.authService.logout();
        this.flashMessage.showFlashMessage({
            messages: ['You are logged out'],
            dismissible: true,
            timeout: 3000,
            type: 'info'
        });
        this.router.navigate(['/login']);
        return false;
    }

}
