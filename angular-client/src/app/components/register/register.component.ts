import {Component, OnInit} from '@angular/core';
import {ValidateService} from '../../services/validate.service';
import {AuthService} from '../../services/auth.service';
import {Router} from "@angular/router";
import {NgFlashMessageService} from 'ng-flash-messages';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    name: String;
    username: String;
    email: String;
    password: String;


    constructor(
        private validateService: ValidateService,
        private authService: AuthService,
        private router: Router,
        private flashMessage: NgFlashMessageService,
    ) {
    }

    ngOnInit() {
    }

    onRegisterSubmit() {
        const user = {
            name: this.name,
            email: this.email,
            username: this.username,
            password: this.password
        };

        // Required fields
        if (!this.validateService.validateRegister(user)) {
            this.flashMessage.showFlashMessage({
                messages: ['Please fill in all fields'],
                dismissible: true,
                timeout: 3000,
                type: 'danger'
            });
            return false;
        }

        // ValidateEmail
        if (!this.validateService.validateEmail(user.email)) {
            this.flashMessage.showFlashMessage({
                messages: ['Please use a valid email'],
                dismissible: true,
                timeout: 3000,
                type: 'danger'
            });
            return false;
        }

        // Register user
        this.authService.registerUser(user).subscribe((data : any) => {
           if (data.success) {
               this.flashMessage.showFlashMessage({
                   messages: ['You are now registered and can log in'],
                   dismissible: true,
                   timeout: 3000,
                   type: 'success'
               });
               this.router.navigate(['/login'])
           } else {
               this.flashMessage.showFlashMessage({
                   messages: ['Something went wrong'],
                   dismissible: true,
                   timeout: 3000,
                   type: 'danger'
               });
               this.router.navigate(['/register'])
           }
        });
    }

}
