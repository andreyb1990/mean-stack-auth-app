# MEAN Stack auth app using Docker

This is a MEAN Stack auth application that can be used as base for your project.

## MongoDB image

Download MongoDB image: `docker pull mongo`

Update the connection string like below:

If using mongo db image:
`database:"mongodb://database/mean-docker"`

If running mongodb remotely (Like hosted db on mlab):
`database: "mongodb://username:password@ds056789.mlab.com:56789/nksmongo"`

## Angular

**angular-client** folder contains the Angular Auth App

## Node.js image

Download Node.js image: `docker pull node:10.7.0-alpine`

## Docker Compose

Run this command to start the project:

`docker-compose up`
