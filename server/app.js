const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');
const app = express();
const port = 3000;

// Database connection
mongoose.connect(config.database, { useNewUrlParser: true });
mongoose.connection.on('connected', () => {
    console.log('Connected to database ' + config.database);
});
mongoose.connection.on('error', (err) => {
    console.log('Database error: ' + err);
});

// Middleware list
app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

// Set static folder
app.use(express.static(path.join(__dirname, 'public')))

// Routes
app.get('/', (req, res) => {
    res.send('Invalid Endpoint');
});

// app.get('*', (req, res) => {
//     res.sendFile(path.join(__dirname, 'public/index.html'))
// });

const users = require('./routes/users');
app.use('/users', users);

// Start server
app.listen(port, () => {
    console.log('Server started on port ' + port);
});
